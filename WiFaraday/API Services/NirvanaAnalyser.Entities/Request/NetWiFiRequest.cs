﻿using NirvanaAnalyser.Entities.Abastract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Entities.Request
{
    /// <summary>
    /// Requisição de dados da rede WIFI
    /// </summary>
    public class NetWiFiRequest : AbstractRequest
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public NetWiFiRequest()
        {
            //Define o identificador
            this.Id = new Random(DateTime.Now.Millisecond + DateTime.Now.Second).Next(2000000);
        }

        /// <summary>
        /// Identificador
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// MAC address da placa de rede
        /// </summary>
        public String MAC { get; set; }

        /// <summary>
        /// Identificador da rede
        /// </summary>
        public String SSID { get; set; }

        /// <summary>
        /// Informações de segurança
        /// </summary>
        public String SecurityInfo { get; set; }

        /// <summary>
        /// Dados de leitura da rede
        /// </summary>
        public ICollection<NetWiFiDataRequest> NetWiFiDataRequestList { get; set; }

        /// <summary>
        /// Dados de leitura da rede
        /// </summary>
        public ICollection<NetWiFiDataRequest> NetWiFiDataRequestListOrdered
        {
            get
            {
                //Verifica se há itens na lista
                if(this.NetWiFiDataRequestList != null)
                {
                    //Retorna items maiores que zero e ordenado pelo data
                    return this.NetWiFiDataRequestList.Where(w => w.EventValue <= 0).OrderBy(o => o.EventDate).ToArray();
                }
                else
                {
                    //Retona vazio
                    return null;
                }
            }
        }
    }
}
