﻿using NirvanaAnalyser.Entities.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Entities.Request
{
    /// <summary>
    /// Dados de leitura da rede
    /// </summary>
    public class NetWiFiDataRequest
    {
        /// <summary>
        /// Data do evento
        /// </summary>
        public long EventDate { get; set; }

        /// <summary>
        /// Data do evento
        /// </summary>
        public string EventDateField {
            get
            {
                //Retorno
                return GenericUtility.GetDateStr(this.EventDate);
            }
        }

        /// <summary>
        /// Valor obtido
        /// </summary>
        public long EventValue { get; set; }
    }
}
