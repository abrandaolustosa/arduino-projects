﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Entities.Entities
{
    /// <summary>
    /// Relatório de erros
    /// </summary>
    public class ErrorReport
    {
        /// <summary>
        /// Código de erro
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Mensagem de erro
        /// </summary>
        public string ErrorInfo { get; set; }
    }
}
