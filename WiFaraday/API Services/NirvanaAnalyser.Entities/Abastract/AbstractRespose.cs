﻿using NirvanaAnalyser.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Entities.Abastract
{
    /// <summary>
    /// Classe abastrata de Resposta
    /// </summary>
    public class AbstractRespose
    {
        /// <summary>
        /// Método construtor
        /// </summary>
        public AbstractRespose()
        {
            //Cria a nova chave
            this.RequestKey = new Guid();
            //Define o insucesso como padrão
            this.Success = false;
        }
        /// <summary>
        /// Chave da requisição
        /// </summary>
        public Guid RequestKey { get; set; }

        /// <summary>
        /// Indica o sucesso na requisição
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Coleção de erros
        /// </summary>
        public IList<ErrorReport> ErrorReportList { get; set; }
    }
}
