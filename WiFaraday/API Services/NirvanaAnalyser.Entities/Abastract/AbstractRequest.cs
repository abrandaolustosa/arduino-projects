﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Entities.Abastract
{
    /// <summary>
    /// Classe abastra de resposta
    /// </summary>
    public class AbstractRequest
    {
        /// <summary>
        /// Método construtor
        /// </summary>
        public AbstractRequest()
        {
            //Gera uma nova chave
            this.RequestKey = Guid.NewGuid();
        }

        /// <summary>
        /// Chave da requisição
        /// </summary>
        [JsonIgnore]
        public Guid RequestKey { get; set; }
    }
}
