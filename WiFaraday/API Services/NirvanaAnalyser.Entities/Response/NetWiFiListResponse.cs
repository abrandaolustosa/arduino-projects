﻿using NirvanaAnalyser.Entities.Abastract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Entities.Request
{
    /// <summary>
    /// Lista de dados de leituras da Rede
    /// </summary>
    public class NetWiFiListResponse : AbstractRespose
    {
        /// <summary>
        /// Leituras da rede
        /// </summary>
        public List<NetWiFiRequest> DataList { get; set; }
    }
}
