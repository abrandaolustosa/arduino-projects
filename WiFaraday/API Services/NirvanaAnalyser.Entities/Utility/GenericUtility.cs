﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Entities.Utility
{
    /// <summary>
    /// Utilitários
    /// </summary>
    public class GenericUtility
    {
        /// <summary>
        /// Converte a data em string
        /// </summary>
        /// <param name="timeSpam">Ticks</param>
        /// <returns></returns>
        public static string GetDateStr(long timeSpam)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);

            dtDateTime = dtDateTime.AddSeconds(timeSpam).ToLocalTime();

            return String.Format("{0:HH:mm:ss}", dtDateTime);
        }
    }
}
