﻿using NirvanaAnalyser.Manager.Processor.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NirvanaAnalyser.Entities.Request;
using NirvanaAnalyser.Entities.Response;
using NirvanaAnalyser.Entities.Entities;

namespace NirvanaAnalyser.Manager.Processor.Stubs
{
    /// <summary>
    /// Simulador de processador da requisição
    /// </summary>
    public class NetWiFiProcessorStub : INetWiFiProcessor
    {
        /// <summary>
        /// Indica se houve erro
        /// </summary>
        public bool _withError { get; set; }

        /// <summary>
        /// Método construtor
        /// </summary>
        /// <param name="withError">Mensagem de erro</param>
        public NetWiFiProcessorStub(bool withError = false)
        {
            //Indica se houve erro
            this._withError = withError;
        }

        /// <summary>
        /// Processa a requisição
        /// </summary>
        /// <param name="netWiFiRequest">Objeto de requisição</param>
        /// <returns>Objeto de resposta</returns>
        public NetWiFiResponse Process(NetWiFiRequest netWiFiRequest)
        {
            //Objeto de resposta
            NetWiFiResponse netWiFiRespose = null;

            //Verifica se é necessário simular erro
            if (this._withError == false)
            {
                //Objeto de resposta
                netWiFiRespose = new NetWiFiResponse()
                {
                    //Lista de erros
                    ErrorReportList = null,
                    //Chave de requisição
                    RequestKey = netWiFiRequest.RequestKey,
                    //Retorno
                    Success = true,
                };
            }
            else
            {
                //Objeto de resposta
                netWiFiRespose = new NetWiFiResponse()
                {
                    //Lista de erros
                    ErrorReportList = new List<ErrorReport>()
                    {
                        //Cria objeto de relatório de erros
                        new ErrorReport()
                        {
                            //Código de erro
                             ErrorCode = 400,
                            //Mensagem de erro
                            ErrorInfo = "Bad Request"
                        },
                    },
                    //Chave de requisição
                    RequestKey = netWiFiRequest.RequestKey,
                    //Retorno
                    Success = false,
                };
            }

            //Retorno
            return netWiFiRespose;
        }

        /// <summary>
        /// Listar dados
        /// </summary>
        /// <param name="mac">Endereço da placa de rede</param>
        /// <returns>Lista de dados</returns>
        public IEnumerable<NetWiFiRequest> ListInfo(string mac)
        {
            //Lista de requisições
            IEnumerable<NetWiFiRequest> netWiFiRequestList = new List<NetWiFiRequest>()
            {
                //Objeto de requisição
                new NetWiFiRequest()
                {
                    //Endereço da placa de rede
                    MAC = "00:1C:B3:09:85:15",
                    //Nome da rede
                    SSID = "Intel_Iot",
                    //Método de segurança
                    SecurityInfo = "WEP",
                    //Dados da rede
                    NetWiFiDataRequestList = new List<NetWiFiDataRequest>()
                    {
                        //Dados da rede
                        new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8100 },
                        //Dados da rede
                        new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8300 },
                        //Dados da rede
                        new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8200 },
                        //Dados da rede
                        new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8160 },
                    }
                },

                //Objeto de requisição
                new NetWiFiRequest()
                {
                    //Endereço da placa de rede
                    MAC = "00:4C:B9:429:85:32",
                    //Nome da rede
                    SSID = "Insper_Iot",
                    //Método de segurança
                    SecurityInfo = "WPA/WPA2",
                    //Dados da rede
                    NetWiFiDataRequestList = new List<NetWiFiDataRequest>()
                    {
                            //Dados da rede
                            new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 7654 },
                            //Dados da rede
                            new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 2213 },
                            //Dados da rede
                            new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 4322 },
                            //Dados da rede
                            new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 5532 },
                    }
                },

                //Objeto de requisição
                new NetWiFiRequest()
                {
                    //Endereço da placa de rede
                    MAC = "00:4C:B9:429:85:32",
                    //Nome da rede
                    SSID = "Insper_Iot",
                    //Método de segurança
                    SecurityInfo = "WPA/WPA2",
                    //Dados da rede
                    NetWiFiDataRequestList = new List<NetWiFiDataRequest>()
                    {
                        //Dados da rede
                        new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 3443 },
                        //Dados da rede
                        new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 4233 },
                        //Dados da rede
                        new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 5435 },
                        //Dados da rede
                        new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 1223 },
                    }
                }
            };

            //Retorno
            return netWiFiRequestList;
        }
    }
}
