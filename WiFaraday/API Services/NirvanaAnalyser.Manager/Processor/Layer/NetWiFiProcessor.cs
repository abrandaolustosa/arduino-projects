﻿using NirvanaAnalyser.Manager.Processor.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NirvanaAnalyser.Entities.Request;
using NirvanaAnalyser.Entities.Response;
using NirvanaAnalyser.Repository.Repository;
using NirvanaAnalyser.Entities.Entities;

namespace NirvanaAnalyser.Manager.Processor.Layer
{
    /// <summary>
    /// Processador
    /// </summary>
    public class NetWiFiProcessor : INetWiFiProcessor
    {
        /// <summary>
        /// Repositório
        /// </summary>
        private NetWiFiRepository _netWiFiRepository = null;

        /// <summary>
        /// Método construtor
        /// </summary>
        public NetWiFiProcessor()
        {
            //Cria orepositório
            this._netWiFiRepository = new NetWiFiRepository();
        }

        /// <summary>
        /// Processador
        /// </summary>
        /// <param name="netWiFiRequest">Objeto de request</param>
        /// <returns>Objeto de resposta</returns>
        public NetWiFiResponse Process(NetWiFiRequest netWiFiRequest)
        {
            //Cria objeto de resposta
            NetWiFiResponse netWiFiResponse = null;

            try
            {
                //Insere o item no repositório
                this._netWiFiRepository.Insert(netWiFiRequest);

                //Define objeto de resposta
                netWiFiResponse = new NetWiFiResponse()
                {
                    //Chave da requisição
                    RequestKey = netWiFiRequest.RequestKey,
                    //Lista de erros
                    ErrorReportList = new List<ErrorReport>(),
                    //Sucesso
                    Success = true,
                };
            }
            catch
            {
                //Define objeto de resposta
                netWiFiResponse = new NetWiFiResponse()
                {
                    //Chave da requisição
                    RequestKey = netWiFiRequest.RequestKey,
                    //Lista de erros
                    ErrorReportList = new List<ErrorReport>(),
                };

                //Grava erro
                netWiFiResponse.ErrorReportList.Add(new ErrorReport()
                {
                    //Código de erro
                    ErrorCode = 400,
                    //Mensagem de erro
                    ErrorInfo = "Bad Request",
                });
            }

            //Retorno
            return netWiFiResponse;
        }


        /// <summary>
        /// Lista o dados do repositório
        /// </summary>
        /// <param name="mac">Endereço da placa de rede</param>
        /// <returns>Lista de requisições</returns>
        public IEnumerable<NetWiFiRequest> ListInfo(string mac)
        {
            //Lista os dados do repositprio
            return this._netWiFiRepository.List(mac);
        }
    }
}
