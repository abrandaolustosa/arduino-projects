﻿using NirvanaAnalyser.Entities.Request;
using NirvanaAnalyser.Entities.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Manager.Processor.Interface
{
    /// <summary>
    /// Processador da requisição
    /// </summary>
    public interface INetWiFiProcessor
    {
        /// <summary>
        /// Processa a requisição
        /// </summary>
        /// <param name="netWiFiRequest">Objeto de requisição</param>
        /// <returns>Objeto de resposta</returns>
        NetWiFiResponse Process(NetWiFiRequest netWiFiRequest);

        /// <summary>
        /// Listar os dados das redes
        /// </summary>
        /// <param name="mac">Endereço da placa de rede</param>
        /// <returns>Listar dados das redes</returns>
        IEnumerable<NetWiFiRequest> ListInfo(string mac);
    }
}
