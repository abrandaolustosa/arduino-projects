﻿using NirvanaAnalyser.Entities.Request;
using NirvanaAnalyser.Entities.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Manager.Interface
{
    /// <summary>
    /// Gerenciador de requisições
    /// </summary>
    public interface INirvanaAnalyserManager
    {
        /// <summary>
        /// Processa a requisição
        /// </summary>
        /// <param name="netWiFiRequest">Objeto de requisição</param>
        /// <returns>Objeto de resposta</returns>        
        NetWiFiResponse ProcessAnalyse(NetWiFiRequest netWiFiRequest);

        /// <summary>
        /// Listar dados de rede
        /// </summary>
        /// <param name="mac">Endereço da placa de rede</param>
        /// <returns></returns>
        NetWiFiListResponse ListInfo(String mac);
    }
}
