﻿using NirvanaAnalyser.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Manager.Validation
{
    /// <summary>
    /// Validação do objeto NetWiFiRequest
    /// </summary>
    public class NetWiFiRequestValidation
    {
        /// <summary>
        /// Objeto de requisição
        /// </summary>
        /// <param name="netWiFiRequest">Resultado</param>
        /// <returns></returns>
        public static bool Validate(NetWiFiRequest netWiFiRequest)
        {
            //Resultado
            bool result = true;

            //Verifica se o objeto é nulo
            if(netWiFiRequest == null)
            {
                //Define como erro
                result = false;
            }else
            {
                //Verifica se os atributos foram preenchidos corretamente
                if((String.IsNullOrWhiteSpace(netWiFiRequest.MAC)) || (String.IsNullOrWhiteSpace(netWiFiRequest.SSID))
                    || (String.IsNullOrWhiteSpace(netWiFiRequest.SecurityInfo))
                        || (netWiFiRequest.NetWiFiDataRequestList == null)
                            || (netWiFiRequest.NetWiFiDataRequestList.Count == 0)){

                    //Retorno
                    return false;
                }
            }

            //Retorno
            return result;
        }
    }
}
