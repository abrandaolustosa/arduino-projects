﻿using NirvanaAnalyser.Manager.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NirvanaAnalyser.Entities.Request;
using NirvanaAnalyser.Entities.Response;
using NirvanaAnalyser.Manager.Processor.Interface;
using NirvanaAnalyser.Manager.Processor.Layer;
using NirvanaAnalyser.Manager.Processor.Stubs;
using NirvanaAnalyser.Entities.Entities;

namespace NirvanaAnalyser.Manager.Layer
{
    /// <summary>
    /// Gerenciador de requisições
    /// </summary>
    public class NirvanaAnalyserManager : INirvanaAnalyserManager
    {
        /// <summary>
        /// Habilita a simulação
        /// </summary>
        private bool _staging = false;

        /// <summary>
        /// Retorna com erro
        /// </summary>
        private bool _stagingWithError = false;

        /// <summary>
        /// Método construtor
        /// </summary>
        public NirvanaAnalyserManager()
        {
            //Desabilita a simulação
            this._staging = false;
            //Sem simulação de erro
            this._stagingWithError = false;
        }

        /// <summary>
        /// Método construtor
        /// </summary>
        public NirvanaAnalyserManager(bool stating, bool stagingWithError)
        {
            //Desabilita a simulação
            this._staging = stating;
            //Sem simulação de erro
            this._stagingWithError = stagingWithError;
        }

        /// <summary>
        /// Processa a requisição
        /// </summary>
        /// <param name="netWiFiRequest">Objeto de requisição</param>
        /// <returns>Objeto de resposta</returns>
        public NetWiFiResponse ProcessAnalyse(NetWiFiRequest netWiFiRequest)
        {
            //Cria objeto de resposta
            NetWiFiResponse netWiFiResponse = new NetWiFiResponse();

            //Processador
            INetWiFiProcessor netWiFiProcessor = null;

            //Verifica se está em modo de teste
            if (this._staging == false)
            {
                //Modo produção
                netWiFiProcessor = new NetWiFiProcessor();
            }
            else
            {
                //Modo de teste
                netWiFiProcessor = new NetWiFiProcessorStub(this._stagingWithError);
            }

            //Processa a requisição
            netWiFiResponse = netWiFiProcessor.Process(netWiFiRequest);

            //Retorno
            return netWiFiResponse;
        }

        /// <summary>
        /// Listar dados de rede
        /// </summary>
        /// <param name="mac">Endereço da placa de rede</param>
        /// <returns></returns>
        public NetWiFiListResponse ListInfo(string mac)
        {
            //Cria objeto de resposta
            NetWiFiListResponse netWiFiListResponse = new NetWiFiListResponse()
            {
                DataList = new List<NetWiFiRequest>(),      //Lista de dados
                ErrorReportList = new List<ErrorReport>(),  //Lista de erros
                RequestKey = Guid.NewGuid(),                //Chave da requisição
                Success = false,                            //Indica se houve sucesso no processamento
            };

            //Processador
            INetWiFiProcessor netWiFiProcessor = null;

            //Verifica se está em modo de teste
            if (this._staging == false)
            {
                //Modo produção
                netWiFiProcessor = new NetWiFiProcessor();
            }
            else
            {
                //Modo de teste
                netWiFiProcessor = new NetWiFiProcessorStub(this._stagingWithError);
            }

            //Processa a requisição
            IEnumerable<NetWiFiRequest> netWiFiRequestList = netWiFiProcessor.ListInfo(mac);

            //Verifica se o objeto é nulo e se há itens
            if((netWiFiRequestList != null) && (netWiFiRequestList.Count() > 0))
            {
                //Define como sucesso
                netWiFiListResponse.Success = true;

                //Acessa cada item
                foreach (var item in netWiFiRequestList)
                {
                    //Adiciona cada item na lista
                    netWiFiListResponse.DataList.Add(item);
                }
            }

            //Retorno
            return netWiFiListResponse;            
        }
    }
}
