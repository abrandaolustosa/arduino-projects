// Morris.js Charts sample data for SB Admin template

$(function() {

    // Area Chart
    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2010 Q1',
            iphone: 2666,
        }, {
            period: '2010 Q2',
            iphone: 2778,
        }, {
            period: '2010 Q3',
            iphone: 4912,
        },],
        xkey: 'period',
        ykeys: ['iphone' ],
        labels: ['iPhone'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
});
