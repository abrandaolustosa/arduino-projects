﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NirvanaAnalyser.Manager.Processor.Interface;
using NirvanaAnalyser.Manager.Layer;
using NirvanaAnalyser.Manager.Interface;
using NirvanaAnalyser.Entities.Request;
using System.Collections.Generic;
using NirvanaAnalyser.Entities.Response;
using Newtonsoft.Json;
using System.Linq;
using NirvanaAnalyser.Repository.Repository;

namespace NirvanaAnalyser.Manager.Tests
{
    /// <summary>
    /// /Classe de teste
    /// </summary>
    [TestClass]
    public class NirvanaAnalyserManagerTest
    {
        #region Staging Test
        /// <summary>
        /// Teste de sucesso em modo staging
        /// </summary>
        [TestMethod]
        public void NetWiFiProcessRequestStagingSuccess()
        {
            //Genreciador
            INirvanaAnalyserManager nirvanaAnalyserManager = new NirvanaAnalyserManager(true, false);

            //Objeto de requisição
            NetWiFiRequest netWiFiRequest = new NetWiFiRequest()
            {
                //Endereço da placa de rede
                MAC = "00:1C:B3:09:85:15",
                //Nome da rede
                SSID = "Intel_Iot",
                //Método de segurança
                SecurityInfo = "WEP",
                //Dados da rede
                NetWiFiDataRequestList = new List<NetWiFiDataRequest>()
                {
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8100 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8300 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8200 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8160 },
                }
            };

            //Objeto de resposta
            NetWiFiResponse netWiFiResponse = nirvanaAnalyserManager.ProcessAnalyse(netWiFiRequest);

            //Verifica se o objeto é nulo
            Assert.IsNotNull(netWiFiResponse);

            //Verifica se houve falha
            Assert.IsTrue(netWiFiResponse.Success);
        }

        [TestMethod]
        public void NetWiFiProcessRequestStagingWithError()
        {
            //Instância o gerenciador
            INirvanaAnalyserManager nirvanaAnalyserManager = new NirvanaAnalyserManager(true, true);

            //Objeto de requisição
            NetWiFiRequest netWiFiRequest = new NetWiFiRequest()
            {
                //Endereço da placa de rede
                MAC = "00:1C:B3:09:85:15",
                //Nome da rede
                SSID = "Intel_Iot",
                //Método de segurança
                SecurityInfo = "WEP",
                //Dados da rede
                NetWiFiDataRequestList = new List<NetWiFiDataRequest>()
                {
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8100 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8300 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8200 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8160 },
                }
            };

            //Objeto de resposta
            NetWiFiResponse netWiFiResponse = nirvanaAnalyserManager.ProcessAnalyse(netWiFiRequest);

            //Verifica se é nulo
            Assert.IsNotNull(netWiFiResponse);

            //Verifica se houve sucesso
            Assert.IsFalse(netWiFiResponse.Success);

            //Verifica se a lista de erros não é nula
            Assert.IsTrue(netWiFiResponse.ErrorReportList != null);

            //Verifica o total de erros
            Assert.IsTrue(netWiFiResponse.ErrorReportList.Count == 1);

            //Verifica o código de erro
            Assert.IsTrue(netWiFiResponse.ErrorReportList[0].ErrorCode == 400);

            //Verifica a mensagem de erro
            Assert.IsTrue(netWiFiResponse.ErrorReportList[0].ErrorInfo == "Bad Request");
        }

        [TestMethod]
        public void NetWiFiListStagingList()
        {
            //Instância o gerenciador
            INirvanaAnalyserManager nirvanaAnalyserManager = new NirvanaAnalyserManager(true, true);

            //Lista de dados
            NetWiFiListResponse netWiFiListResponse = nirvanaAnalyserManager.ListInfo("00:1C");

            Assert.IsNotNull(netWiFiListResponse);

            Assert.IsTrue(netWiFiListResponse.Success);
        }
        #endregion

        #region Production Test
        /// <summary>
        /// Teste de sucesso em modo staging
        /// </summary>
        [TestMethod]
        public void NetWiFiProcessRequestProductionSuccess()
        {
            //Genreciador
            INirvanaAnalyserManager nirvanaAnalyserManager = new NirvanaAnalyserManager();

            #region Objeto de requisição
            //Objeto de requisição
            NetWiFiRequest netWiFiRequest = new NetWiFiRequest()
            {
                //Endereço da placa de rede
                MAC = "00:1C:B3:09:85:15",
                //Nome da rede
                SSID = "Intel_Iot",
                //Método de segurança
                SecurityInfo = "WEP",
                //Dados da rede
                NetWiFiDataRequestList = new List<NetWiFiDataRequest>()
                {
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8100 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8300 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8200 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8160 },
                }
            };
            #endregion

            //Objeto de resposta
            NetWiFiResponse netWiFiResponse = nirvanaAnalyserManager.ProcessAnalyse(netWiFiRequest);

            //Verifica se o objeto é nulo
            Assert.IsNotNull(netWiFiResponse);

            //Verifica se houve falha
            Assert.IsTrue(netWiFiResponse.Success);
        }
        #endregion

        #region Repository Test
        /// <summary>
        /// Teste de repositorio
        /// </summary>
        [TestMethod]
        public void NetWifiRepositoryListInsertTest()
        {
            //Nome da rede
            String mac = "00:1C:B3:09:85:15";

            //Processador do repositorio
            NetWiFiRepository repositoryProcess = new NetWiFiRepository();

            //Objeto de requisição
            NetWiFiRequest netWiFiRequest = new NetWiFiRequest()
            {
                //Endereço da placa de rede
                MAC = mac,
                //Nome da rede
                SSID = "Intel_Iot",
                //Método de segurança
                SecurityInfo = "WEP",
                //Dados da rede
                NetWiFiDataRequestList = new List<NetWiFiDataRequest>()
                {
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8100 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8300 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8200 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8160 },
                }
            };

            //Contagem antes
            int countBefore = repositoryProcess.List(mac).Count();

            //Insere os dados
            repositoryProcess.Insert(netWiFiRequest);

            //Contagem depois
            int countAfter = repositoryProcess.List(mac).Count();

            //Verificação
            Assert.IsTrue(countBefore < countAfter);
        }
        #endregion

        #region Conversion Tests
        [TestMethod]
        public void NetWifiRequestToJson()
        {
            //Objeto de requisição
            NetWiFiRequest netWiFiRequest = new NetWiFiRequest()
            {
                //Endereço da placa de rede
                MAC = "00:1C:B3:09:85:15",
                //Nome da rede
                SSID = "Intel_Iot",
                //Método de segurança
                SecurityInfo = "WEP",
                //Dados da rede
                NetWiFiDataRequestList = new List<NetWiFiDataRequest>()
                {
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8100 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8300 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8200 },
                    //Dados da rede
                    new NetWiFiDataRequest() { EventDate = DateTime.Now.Ticks, EventValue = 8160 },
                }
            };

            //Cria objeto jSon
            String jsonData = JsonConvert.SerializeObject(netWiFiRequest);
        }
        #endregion
    }
}
