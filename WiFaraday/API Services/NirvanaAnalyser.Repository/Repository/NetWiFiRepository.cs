﻿using NirvanaAnalyser.Entities.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NirvanaAnalyser.Repository.Repository
{
    /// <summary>
    /// Repositório de dados
    /// </summary>
    public class NetWiFiRepository
    {
        /// <summary>
        /// Cache
        /// </summary>
        private static HashSet<NetWiFiRequest> _database = null;

        /// <summary>
        /// Objeto de sincronismo
        /// </summary>
        private object syncObject = new object();

        /// <summary>
        /// Método contsrutor
        /// </summary>
        public NetWiFiRepository()
        {
            //Verifica se o objeto é nulo
            if(NetWiFiRepository._database == null)
            {
                //Efetua o lock do objeto
                lock (syncObject)
                {
                    //Veirifica se o objeto é nulo
                    if (NetWiFiRepository._database == null)
                    {
                        //Cria a instância do objeto
                        NetWiFiRepository._database = new HashSet<NetWiFiRequest>();
                    }
                }
            }
        }

        /// <summary>
        /// PErsiste os dados no chache
        /// </summary>
        /// <param name="netWiFiRequest">Dados da requisição</param>
        public void Insert(NetWiFiRequest netWiFiRequest)
        {
            //Persiste os dados na base
            NetWiFiRepository._database.Add(netWiFiRequest);
        }

        /// <summary>
        /// Obtem dados de uma rede
        /// </summary>
        /// <param name="MACFilter">Pesquisa de redes</param>
        /// <returns></returns>
        public IEnumerable<NetWiFiRequest> List(String MACFilter)
        {
            //Retorna os dados da lista encontrada
            return NetWiFiRepository._database.Where(w => w.MAC.Contains(MACFilter));
        }
    }
}