﻿using NirvanaAnalyser.Entities.Entities;
using NirvanaAnalyser.Entities.Request;
using NirvanaAnalyser.Entities.Response;
using NirvanaAnalyser.Manager.Interface;
using NirvanaAnalyser.Manager.Layer;
using NirvanaAnalyser.Manager.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Cors;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NirvanaAnalyser.AnalyserAPI.Controllers
{
    /// <summary>
    /// API de dados de rede WiFi
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NetWiFiAnalyserController : ApiController
    {
        /// <summary>
        /// Objeto de gerenciamento
        /// </summary>
        private INirvanaAnalyserManager _nirvanaAnalyserManager { get; set; }

        /// <summary>
        /// Método construtor
        /// </summary>
        public NetWiFiAnalyserController()
        {
            //Inicializa o gerenciador
            //this._nirvanaAnalyserManager = new NirvanaAnalyserManager(true, false);
            this._nirvanaAnalyserManager = new NirvanaAnalyserManager();
        }

        /// <summary>
        /// Atualização dos dados
        /// </summary>
        /// <param name="netWiFiRequest">Objeto de requisição</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Upload(NetWiFiRequest netWiFiRequest)
        {
            //Objeto de resposta
            HttpResponseMessage httpResponseMessage = null;

            //Objeto de resposta de negócio
            NetWiFiResponse netWiFiResponse = new NetWiFiResponse()
            {
                ErrorReportList = new List<ErrorReport>(),  //Lista de erros
                RequestKey = Guid.NewGuid(),                //Novo identificador
                Success = false,                            //Inicializa com insucesso
            };

            try
            {
                //Validação
                if (NetWiFiRequestValidation.Validate(netWiFiRequest))
                {
                    //Processa os dados
                    netWiFiResponse = this._nirvanaAnalyserManager.ProcessAnalyse(netWiFiRequest);

                    //Prepara a resposta
                    httpResponseMessage = Request.CreateResponse<NetWiFiResponse>(HttpStatusCode.OK, netWiFiResponse);
                }
                else
                {
                    //Define erro de validação
                    httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
            }
            catch
            {
                //Erro interno da API
                httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

            //Retorno
            return httpResponseMessage;
        }

        /// <summary>
        /// Listar dados
        /// </summary>
        /// <param name="mac"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage List(String mac)
        {
            //Objeto de resposta
            HttpResponseMessage httpResponseMessage = null;

            //Verifica se o mac foi informado
            if (String.IsNullOrWhiteSpace(mac))
            {
                //Define o valor padrão
                mac = String.Empty;
            }

            try
            {
                //Processa os dados
                NetWiFiListResponse netWiFiListResponse = this._nirvanaAnalyserManager.ListInfo(mac);

                //Prepara a resposta
                httpResponseMessage = Request.CreateResponse<NetWiFiListResponse>(HttpStatusCode.OK, netWiFiListResponse);
            }
            catch
            {
                //Erro interno da API
                httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

            //Retorno
            return httpResponseMessage;
        }
    }
}
